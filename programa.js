class Sembrador {
    campo;
    campoCantidadMinas;
    tamaño;
    cantidadMinas;


    constructor(){
        this.tamaño=10;
        this.campo = Array();
        this.cantidadMinas=Math.round(Math.pow(this.tamaño,2)*0.1);
    }

    crearCampo()
    {   
        contiguo:[]
        for(let i=0; i<this.tamaño;i++)
        {
            for(let j=0;j<this.tamaño;j++){
                if(!this.campo[i]){
                    this.campo[i]=Array();
                }
                this.campo[i][j]=0;
            }
        }
    }


    sembrarMinas() 
    {        

        while(this.cantidadMinas>0){
            let fila=Math.floor(Math.random()*this.tamaño);
            let columna=Math.floor(Math.random()*this.tamaño);

            if(this.campo[fila][columna]===0){
                this.campo[fila][columna]=1;
                this.cantidadMinas--;
            }
        }        
    }


    contarMinasAlrededor()
    {   
        for(i=0;i<this.tamaño;i++)
        {
            let cuadro=this.campo[i];

            if(cuadro.columna==1)
            {
                if(cuadro.fila==1)
                {
                     cuadro.contiguo.push(i + 1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas + 1);
                }
                else if(cuadro.fila == fila)
                {
                    cuadro.contiguo.push(i + 1);
                    cuadro.contiguo.push(i - columnas);
                    cuadro.contiguo.push(i - columnas + 1);
                }
                else
                {
                    cuadro.contiguo.push(i+1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas + 1);
                    cuadro.contiguo.push(i - columnas);
                    cuadro.contiguo.push(i - columnas + 1);
                }
            }
            else if (cuadro.columna==columnas)
            {
                if(cuadro.fila == 1)
                {
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas - 1);
                }
                else if(cuadro.fila == filas)
                {
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i - columnas)
                    cuadro.contiguo.push(i - columnas - 1)
                }
                else
                {
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas - 1);
                    cuadro.contiguo.push(i - columnas);
                    cuadro.contiguo.push(i - columnas - 1);
                }
            }
            else
            {
                if(cuadro.fila == 1)
                {
                    cuadro.contiguo.push(i + 1);
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i + columnas - 1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas + 1);
                }
                else if (cuadro.fila == filas)
                {
                    cuadro.contiguo.push(i + 1);
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i - columnas - 1);
                    cuadro.contiguo.push(i - columnas);
                    cuadro.contiguo.push(i - columnas + 1);
                }
                else
                {
                    cuadro.contiguo.push(i + 1);
                    cuadro.contiguo.push(i - 1);
                    cuadro.contiguo.push(i + columnas - 1);
                    cuadro.contiguo.push(i + columnas);
                    cuadro.contiguo.push(i + columnas + 1);
                    cuadro.contiguo.push(i - columnas - 1);
                    cuadro.contiguo.push(i - columnas);
                    cuadro.contiguo.push(i - columnas + 1);
                }
            }

            if (cuadro.valor != 1)
            {
                let minas=cuadro.contiguo.filter(v => this.campo[v].valor= 1).length;
                if (minas > 0)
                {
                    cuadro.valor= minas;
                }
            }
        }
        if (campo.valor==1)
        {
            // explocion
        }
        else if(this.campo.valor=0)
        {
            this.campo.contiguo.forEach(v => {
                this.contarMinasAlrededor(this.campo[v]);
            });
        }

    }

    set tamañoUsuario(valor)
    {
        if(valor<4){
        this.tamaño=4;
        }else{
            this.tamaño=valor;
        }
    }

    set cantidadMinasUsuario(valor){

        valor=Math.round(valor);        

        
        if(valor<1){
            this.cantidadMinas=2;
            }else{
                this.cantidadMinas=valor;
            }
    }
}

let miSembrador = new Sembrador();

miSembrador.tamañoUsuario=10;
miSembrador.cantidadMinasUsuario=20;
miSembrador.crearCampo();
miSembrador.sembrarMinas();
